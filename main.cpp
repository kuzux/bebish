#include "pch.h"

#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#include <signal.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>

#ifdef __APPLE__
#include <mach-o/dyld.h>
#endif

#include <readline/readline.h>
#include <readline/history.h>

#include "common.h"
#include "shell_parser.h"
#include "shell.h"
#include "path.h"

/*void sigint_handler(int _sig) { (void)_sig; }

int main(int argc, char** argv, char** envp) {
    fmt::print("Bebish {}\n", Bebish::version);

    struct sigaction sa;
    sa.sa_handler = sigint_handler;
    sigaction(SIGINT, &sa, NULL);

    Bebish::Path::load_path();
    Bebish::load_parent_env(argv, envp);
    using_history();

    while(!feof(stdin)) {
        Bebish::reap_bg_jobs();

        // TODO: Custom completion function for all things in the path
        char* cmdline = readline("> ");
        if(!cmdline) break; // we got an eof

        size_t len = strlen(cmdline);
        if(len == 0) continue; // we have an empty line
        if(len > 0) add_history(cmdline);

        Bebish::Shell::Parser parser({ cmdline, len });
        auto parse_result = parser.parse_pipeline();
        if(std::holds_alternative<Bebish::ParseError>(parse_result)) {
            Bebish::ParseError error = std::get<Bebish::ParseError>(parse_result);
            fmt::print(stderr, "Parse error: {}\n", error.message);
            continue;
        }
        Bebish::CmdPipeline pipeline = std::get<Bebish::CmdPipeline>(parse_result);
        if(pipeline.cmds.empty()) continue;

        if(Bebish::do_builtins(pipeline.cmds[0])) continue;
        Bebish::PipelineResult res = Bebish::run_pipeline(pipeline, { cmdline, len });
        Bebish::report_pipeline_result(res);

        free(cmdline); // readline allocates memory and we have to free it
    }

    // TODO: What happens if we run out of stdin but still have running bg jobs?

    clear_history();
    Bebish::unload_parent_env();

    return 0;
}*/

namespace Bebish::Lang {

enum class TokenType {
    Identifier, String, Number, Cmdline,
    True, False, Null,

    Do, End, If, Then, Else, While, Fn,

    Assign, LeftParen, RightParen, Comma, Semicolon,
    Plus, Minus, Multiply, Divide, And, Or, Not,
    Equals, NotEquals, LessThan, GreaterThan,
    LessThanEqual, GreaterThanEqual,

    Eof
};

struct Token {
    TokenType type;

    // offsets to the input text
    size_t start;
    size_t end;
};

class Tokenizer {
public:
    Tokenizer(std::string_view input) : input(input) {}

    // TODO: Return possible errors as well
    std::vector<Token> const& tokenize() {
        while(curr < input.length()) {
            start = curr;
            scanToken();
        }
        tokens.push_back({ TokenType::Eof, input.length(), input.length() });

        return tokens;
    }
private:
    std::string_view input;
    std::vector<Token> tokens;

    // where are we currently in the input text
    size_t start = 0;
    size_t curr = 0;

    void scanToken() {
        auto ch = advance();
        if(isspace(ch)) return;

        switch(ch) {
            case '(': addToken(TokenType::LeftParen); break;
            case ')': addToken(TokenType::RightParen); break;

            case '+': addToken(TokenType::Plus); break;
            case '-': addToken(TokenType::Minus); break;
            case '*': addToken(TokenType::Multiply); break;
            case '/': addToken(TokenType::Divide); break;

            case ',': addToken(TokenType::Comma); break;
            case ';': addToken(TokenType::Semicolon); break;

            case '!':
                if(match('=')) addToken(TokenType::NotEquals);
                else addToken(TokenType::Not);
                break;

            case ':':
                if(match('=')) addToken(TokenType::Assign);
                // Handle the error case here
                break;

            case '<':
                if(match('=')) addToken(TokenType::LessThanEqual);
                else addToken(TokenType::LessThan);
                break;
            case '>':
                if(match('=')) addToken(TokenType::GreaterThanEqual);
                else addToken(TokenType::GreaterThan);
                break;

            case '&':
                if(match('&')) addToken(TokenType::And);
                break;
            case '|':
                if(match('|')) addToken(TokenType::Or);
                break;

            case '#': // comments
                while(peek() != '\n' && curr < input.length()) advance();
                break;

            case '"': string(); break;
            case '`': cmdline(); break;

            default:
                if(isnumber(ch)) number();
                else if(isalnum(ch)) identifier();
                // TODO: Handle error here as well
                break;
        }
    }

    void string() {
        // TODO: Handle escaped quotes
        while(peek() != '"' && curr < input.length()) advance();

        // TODO: if(curr == input.length()) handle unterminated string
        advance();
        addToken(TokenType::String);
    }

    void cmdline() {
        // TODO: Handle escaped quotes
        while(peek() != '`' && curr < input.length()) advance();

        // TODO: if(curr == input.length()) handle unterminated string
        advance();
        addToken(TokenType::Cmdline);
    }

    void number() {
        while(isnumber(peek())) advance();

        if(peek() == '.') {
            advance();
            while(isnumber(peek())) advance();
        }

        addToken(TokenType::Number);
    }

    void identifier() {
        static std::unordered_map<std::string_view, TokenType> reserved_words = {
            { "do", TokenType::Do },
            { "end", TokenType::End },
            { "if", TokenType::If },
            { "then", TokenType::Then },
            { "else", TokenType::Else },
            { "while", TokenType::While },
            { "fn", TokenType::Fn },
        };

        while(isalnum(peek())) advance();
        std::string_view ident = input.substr(start, curr-start);
        if(reserved_words.find(ident) != reserved_words.end()) {
            addToken(reserved_words.at(ident));
            return;
        }
        addToken(TokenType::Identifier);
    }

    char advance() {
        return input[curr++];
    }

    char peek() {
        if(curr == input.length()) return '\0';
        return input[curr];
    }

    bool match(char expected) {
        if(curr == input.length()) return false;
        if(input[curr] != expected) return false;

        curr++;
        return true;
    }

    void addToken(TokenType type) {
        tokens.push_back({ type, start, curr });
    }
};

}

std::string token_type_to_str(Bebish::Lang::TokenType type) {
    static std::vector<std::string> names = {
    "Identifier", "String", "Number", "Cmdline",
    "True", "False", "Null",

    "Do", "End", "If", "Then", "Else", "While", "Fn",

    "Assign", "LeftParen", "RightParen", "Comma", "Semicolon",
    "Plus", "Minus", "Multiply", "Divide", "And", "Or", "Not",
    "Equals", "NotEquals", "LessThan", "GreaterThan",
    "LessThanEqual", "GreaterThanEqual",

    "Eof" };

    return names[(int)type];
}


int main(int argc, char** argv) {
    fmt::print("BebishLang 0.0\n", Bebish::version);
    using_history();

    while(!feof(stdin)) {
        char* input = readline("> ");
        if(!input) break; // we got an eof

        size_t len = strlen(input);
        if(len == 0) continue; // we have an empty line

        std::string_view sv_input { input, len };

        Bebish::Lang::Tokenizer tokenizer(sv_input);
        for(auto& tok : tokenizer.tokenize()) {
            std::string_view str = sv_input.substr(tok.start, tok.end-tok.start);
            fmt::print("{} {} {}\n", token_type_to_str(tok.type), str, str.length());
        }

        free(input); // readline allocates memory and we have to free it
    }

    clear_history();
    return 0;
}
