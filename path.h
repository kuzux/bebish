#pragma once

#include "pch.h"

namespace Bebish::Path {

void load_path();

// returns the absolute path found
// returns empty string if not found
std::string search_in_path(std::string_view executable);

}