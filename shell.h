#pragma once

#include "pch.h"
#include "common.h"

#include <unistd.h>

namespace Bebish {

struct PipelineResult {
    enum class Type {
        Finished,
        Error,
        BgJob
    };

    Type type;

    // only if type == Error
    int error_code;
    std::string error_message;

    // valid for type == Finished and type == BgJob
    std::vector<pid_t> child_pids;

    std::vector<bool> finished;
    std::vector<int> exit_statuses;

    // only valid for type == BgJob
    int bg_job_id = 0;
    pid_t pgid = 0;

    // valid for finished & bgjob
    std::string cmdline;

    // args are as returned by waitpid
    // pid = waitpid(... , &status, ...)
    static PipelineResult success(std::string_view cmdline, std::vector<pid_t>&& pids, std::vector<int>&& statuses);
    static PipelineResult background(std::string_view cmdline, std::vector<pid_t>&& pids, pid_t pgid);
    static PipelineResult perror(std::string_view cmd, std::string_view cause);

    static PipelineResult with_error_message(std::string_view cmd, std::string_view message);

    static PipelineResult perror(std::string_view cause);

    static PipelineResult with_error_message(std::string_view message);

private:
    PipelineResult(std::string_view cmdline, std::vector<pid_t>&& pids, std::vector<int>&& statuses);
    PipelineResult(std::string_view cmdline, std::vector<pid_t>&& pids, pid_t pgid);
    PipelineResult(int error_code, std::string_view message);
};

// returns new bg job id
// TODO: Create a separate BackgroundJob structure to sort out this mess
// TODO: Separate bg job handling to its own namespace
int create_bg_job(PipelineResult&& job);
void report_and_remove_bg_job(int job_id);
void reap_bg_jobs();

inline std::string_view get_program_name(const CmdParams& params) {
    return { (char*)params.cmdline[0].data(), params.cmdline[0].size() };
}

// creates copies of the arguments
void load_parent_env(char** argv, char** envp);
void unload_parent_env();

struct RedirectIo {
    // all file descriptors can be 0, which means they are not redirected
    int stdin_fd;
    int stdout_fd;
    int stderr_fd;
};

// stdin, stdout, stderr can be 0 (not redirected if 0)
// does not close descriptors
// pgid can be 0, in which case a new process group is created with the same pid as the child process
// (return value becomes both child pid and child pgid)
// takes all_fds because it needs to vlose all the inherited fd s in the child process
// return value behaves like fork()
pid_t do_spawn_process(const std::string& executable, CmdParams& cmd, int pgid, RedirectIo redirect, const std::vector<int>& all_fds);

// sets errno if we can't execute the file
bool can_execute_file(const std::string& path);

PipelineResult do_wait_for_fg_pipeline(std::string_view cmdline, std::vector<pid_t>&& pids, pid_t pgid);
PipelineResult run_pipeline(CmdPipeline& pipeline, std::string_view cmdline);
void report_pipeline_result(const PipelineResult& res);

// returns true if any builtins were ran
// TODO: Separate namespace/file for builtins as well
bool do_builtins(const CmdParams& args);

}