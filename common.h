#pragma once

#include <vector>

namespace Bebish {
    constexpr const char* version = "0.2";

    typedef std::vector<char> CmdPart;
    typedef std::vector<CmdPart> CmdLine;
    // key value pair
    typedef std::vector<std::pair<CmdPart, CmdPart>> EnvVars;

    struct CmdParams {
        std::vector<CmdPart> cmdline;

        EnvVars env_vars;
    };

    struct CmdPipeline {
        std::vector<CmdParams> cmds;

        // empty if not redirected
        CmdPart redirect_stdin;
        CmdPart redirect_stdout;

        // redirect stderr as well as stdout
        bool redirect_stderr = false;
        // append stdout (or stderr) to file
        bool append_stdout = false;
        bool bg_job = false;
    };
    struct ParseError {
        // TODO: Report error locations as well
        std::string message;
    };

}
