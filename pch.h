#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <vector>
#include <string>
#include <string_view>
#include <fmt/ranges.h>
#include <variant>
#include <optional>
#include <unordered_map>
#include <queue>
