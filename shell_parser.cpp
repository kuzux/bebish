#include "pch.h"

#include "shell_parser.h"

namespace Bebish::Shell {

std::vector<Token> tokenize(std::string_view cmdline) {
    std::vector<Token> tokens;
    std::vector<char> curr_token;

    // Using a finite state machine
    enum { start, ident, special, in_quote } 
        state = start;

    auto is_special = [](char c) {
        return (c == '<' || c == '>' || c == '&' || c == '|' || c == '=');
    };

    auto curr_token_match = [&](std::string_view&& str) {
        if(curr_token.size() != str.length()) return false;

        for(size_t i=0; i<curr_token.size(); i++) 
            if(curr_token[i] != str[i]) return false;
        return true;
    };

    auto add_new_token = [&]() {
        if(curr_token.empty()) return;
        if(curr_token[0] == '\0') return;

        TokenType type = TokenType::Plain;
        if(state == special) {
            if(curr_token_match("<"))
                type = TokenType::RedirectIn;
            else if(curr_token_match(">"))
                type = TokenType::RedirectOut;
            else if(curr_token_match("|"))
                type = TokenType::Pipe;
            else if(curr_token_match("&"))
                type = TokenType::Ampersand;
            else if(curr_token_match("="))
                type = TokenType::Equals;
            else if(curr_token_match(">>"))
                type = TokenType::RedirectOutApp;
            else if(curr_token_match("&>"))
                type = TokenType::RedirectErr;
            else if(curr_token_match("&>>"))
                type = TokenType::RedirectErrApp;
        }

        if(curr_token[curr_token.size()-1]!='\0') curr_token.push_back('\0');
        Token t = {.type = type, .text = std::move(curr_token)};
        tokens.push_back(std::move(t));
    };

    for(size_t i=0; i<cmdline.length(); i++) {
        char c = cmdline[i];
        if(state == start) {
            if(isspace(c)) continue;
            if(c =='"') {
                state = in_quote;
                continue;
            }
            curr_token.push_back(c);
            if(is_special(c)) state = special;
            else state = ident;
        } else if(state == ident) {
            if(c == '\\') {
                if(i+1 == cmdline.length()) continue;
                char n = cmdline[i+1];
                curr_token.push_back(n);
                i++;
                continue;
            } else if(isspace(c)) {
                add_new_token();
                state = start;
                continue;
            }
            if(c == '"') {
                state = in_quote;
                continue;
            }
            if(is_special(c)) {
                add_new_token();
                state = special;
                curr_token.push_back(c);
                continue;
            }
            curr_token.push_back(c);
        } else if(state == special) {
            if(isspace(c)) {
                add_new_token();
                state = start;
                continue;
            }
            if(c == '"') {
                add_new_token();
                state = in_quote;
                continue;
            }
            if(!is_special(c)) {
                add_new_token();
                state = ident;
                curr_token.push_back(c);
                continue;
            }
            curr_token.push_back(c);
        } else if(state == in_quote) {
            if(c == '\\') {
                if(i+1 == cmdline.length()) continue;
                char n = cmdline[i+1];
                curr_token.push_back(n);
                i++;
                continue;
            } else if(c == '"') {
                // I'm not sure if this is the "correct" behavior in cases like
                // "asd q"we
                add_new_token();
                state = start;
                continue;
            }
            curr_token.push_back(c);
        }
    }

    add_new_token();

    return tokens;
}

std::variant<CmdParams, ParseError> Parser::parse_command() {
    CmdParams res;

    for(;;) {
        auto maybe_tok = lookahead();
        if(!maybe_tok) break;
        auto& tok = maybe_tok->get();

        if(tok.type == TokenType::Plain) {
            auto mutable_tok = consume()->get();
            auto maybe_next = lookahead();
            if(maybe_next && maybe_next->get().type == TokenType::Equals) {
                auto& key = mutable_tok;
                consume();
                auto maybe_value = consume();
                if(!maybe_value) return ParseError { "Expected another token after equals sign" };
                auto& value = maybe_value->get();
                if(value.type != TokenType::Plain)
                    return ParseError { fmt::format("Unexpected token {} after equals sign", value.text.data()) };

                res.env_vars.push_back({ std::move(key.text), std::move(value.text) });
                continue;
            }

            res.cmdline.push_back(std::move(mutable_tok.text));
        } else {
            break;
        }
    }

    // TODO: Might need to add curr token here
    return res;
}

// std::vector<char> s are null terminated in the return value
std::variant<CmdPipeline, ParseError> Parser::parse_pipeline() {
    // pipeline ::= command (| command)* redirects

    if(tokens.empty()) return CmdPipeline {};

    CmdPipeline res;

    auto first_cmd = parse_command();
    if(std::holds_alternative<ParseError>(first_cmd)) return std::get<ParseError>(first_cmd);
    res.cmds.push_back(std::get<CmdParams>(first_cmd));

    for(;;) {
        auto next = lookahead();
        if(!next || next->get().type != TokenType::Pipe) break;
        consume();

        auto next_cmd = parse_command();
        if(std::holds_alternative<ParseError>(next_cmd)) return std::get<ParseError>(next_cmd);
        res.cmds.push_back(std::get<CmdParams>(next_cmd));
    }

    // parsing the io redirects
    for(;;) {
        auto maybe_tok = consume();
        if(!maybe_tok) break;
        auto& tok = maybe_tok->get();

        if(tok.type == TokenType::RedirectIn) {
            auto maybe_next = consume();
            if(!maybe_next) return ParseError { "Expected another token" };
            auto& next = maybe_next->get();
            if(next.type != TokenType::Plain)
                return ParseError { fmt::format("Unexpected token {} after input redirection", next.text.data()) };

            res.redirect_stdin = std::move(next.text);
        } else if(tok.type == TokenType::RedirectOut) {
            auto maybe_next = consume();
            if(!maybe_next) return ParseError { "Expected another token" };
            auto& next = maybe_next->get();
            if(next.type != TokenType::Plain)
                return ParseError { fmt::format("Unexpected token {} after output redirection", next.text.data()) };

            res.redirect_stdout = std::move(next.text);
        } else if(tok.type == TokenType::RedirectOutApp) {
            auto maybe_next = consume();
            if(!maybe_next) return ParseError { "Expected another token" };
            auto& next = maybe_next->get();
            if(next.type != TokenType::Plain)
                return ParseError { fmt::format("Unexpected token {} after output redirection", next.text.data()) };

            res.redirect_stdout = std::move(next.text);
            res.append_stdout = true;
        } else if(tok.type == TokenType::RedirectErr) {
            auto maybe_next = consume();
            if(!maybe_next) return ParseError { "Expected another token" };
            auto& next = maybe_next->get();
            if(next.type != TokenType::Plain)
                return ParseError { fmt::format("Unexpected token {} after output redirection", next.text.data()) };

            res.redirect_stdout = std::move(next.text);
            res.redirect_stderr = true;
        } else if(tok.type == TokenType::RedirectErrApp) {
            auto maybe_next = consume();
            if(!maybe_next) return ParseError { "Expected another token" };
            auto& next = maybe_next->get();
            if(next.type != TokenType::Plain)
                return ParseError { fmt::format("Unexpected token {} after output redirection", next.text.data()) };

            res.redirect_stdout = std::move(next.text);
            res.redirect_stderr = true;
            res.append_stdout = true;
        } else if(tok.type == TokenType::Ampersand) {
            res.bg_job = true;
        } else {
            return ParseError { fmt::format("Unexpected token {}", tok.text.data()) };
        }
    }

    return res;
}

}