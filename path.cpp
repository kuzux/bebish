#include "pch.h"
#include "path.h"

#include <dirent.h>

namespace Bebish::Path {

std::vector<char*> path;
void load_path() {
    char* opath = getenv("PATH");
    if(!opath) return;
    char* copy = strdup(opath);
    if(!copy) return;

    char* curr_dir = strtok(copy, ":");
    while(curr_dir) {
        path.push_back(curr_dir);
        curr_dir = strtok(NULL, ":");
    }

    // the copy is never freed since it contains path entries
}

// returns the absolute path found
// returns empty string if not found
std::string search_in_path(std::string_view executable) {
    if(path.empty()) return "";

    // TODO: We might want to cache this lookup
    auto matches_exec = [&](const char* s) {
        for(size_t i=0; i<executable.length(); i++) {
            if(executable[i] != *s) return false;
            // do not increment in case the final chra of the string view is a null
            if(executable[i]) s++;
        }
        // make sure the compared string ends here
        if(*s) return false;
        return true;
    };

    for(char* path_entry : path) {
        // fmt::print("searching in {}\n", path_entry);
        DIR* dir = opendir(path_entry);
        if(!dir) continue;

        struct dirent* entry;
        while((entry = readdir(dir)) != NULL) {
            // if(!strcmp(path_entry, "/usr/bin")) printf("Looking for %s in \"%s/%s\"\n", copy, path_entry, entry->d_name);

            if(matches_exec(entry->d_name)) {
                closedir(dir);
                return fmt::format("{}/{}", path_entry, executable);
            }
        }

        closedir(dir);
    }

    return "";
}

}