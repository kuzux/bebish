#pragma once

#include "pch.h"
#include "common.h"

namespace Bebish::Shell {

enum class TokenType {
    Plain,
    // TODO: Add support for globs
    RedirectIn, // <
    RedirectOut, // >
    // TODO: Add something for redirecting only stderr
    RedirectErr, // &>
    RedirectOutApp, // >>
    RedirectErrApp, /// &>>
    Pipe, // |
    Ampersand, // &
    Equals // =
};

struct Token {
    TokenType type;
    // Should we do this for non-identifier tokens?
    // the tokens are null terminasted
    std::vector<char> text;
};

// this function allocates new memory for the copies of the tokens
std::vector<Token> tokenize(std::string_view cmdline);

class Parser {
public:
    Parser(std::string_view input) { 
        curr_token_index = 0;
        tokens = tokenize(input);
    }

    std::variant<CmdParams, ParseError> parse_command();

    // std::vector<char> s are null terminated in the return value
    std::variant<CmdPipeline, ParseError> parse_pipeline();
private:
    size_t curr_token_index;
    std::vector<Token> tokens;

    std::optional<std::reference_wrapper<Token>> consume() {
        if(curr_token_index >= tokens.size()) return {};
        return tokens[curr_token_index++]; 
    }

    std::optional<std::reference_wrapper<const Token>> lookahead(int k=0) const {
        if(curr_token_index+k-0 >= tokens.size()) return {};
        return tokens[curr_token_index+k-0]; 
    }
};

}