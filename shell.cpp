#include "pch.h"
#include "shell.h"
#include "path.h"

#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#include <signal.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>

#ifdef __APPLE__
#include <mach-o/dyld.h>
#endif

std::optional<std::string> get_current_executable() {
#if defined(__APPLE__)
    // https://stackoverflow.com/a/7010480
    char buf[PATH_MAX];
    uint32_t bufsize = PATH_MAX;
    if(!_NSGetExecutablePath(buf, &bufsize))
        return buf;
    return {};
#elif defined(__linux__)
    char buf[PATH_MAX+1];
    ssize_t len = readlink("/proc/self/exe", buf, PATH_MAX);
    if(len < 0) return {};
    buf[len] = '\0';
    if(!strcmp(buf+len-10, " (deleted)")) {
        len -= 10;
        buf[len] = '\0';
    }

    return std::string(buf, len);
#endif
// TODO: Implement this on other platforms as well
// See https://stackoverflow.com/a/1024937 with links
// https://stackoverflow.com/q/799679 specifically for freebsd

    // Could not find it
    return {};
    // TODO: Implement a fallback with getting path of curr dir at startup
    // And appending argv[0] to it
}

// waits for any child to exit
// returns (pid, status)
// pid < 0 on error
std::pair<pid_t, int> do_wait_child(pid_t pid = -1) {
    int status;
    pid_t res_pid = waitpid(pid, &status, WUNTRACED);
    return { res_pid, status };
}


namespace Bebish {

std::unordered_map<int, PipelineResult> bg_jobs;
std::unordered_map<pid_t, int> child_pid_to_job_id;

// returns new bg job id
int create_bg_job(PipelineResult&& job) {
    static int id = 1;
    // No need to reuse job id s
    int curr_id = id++;
    job.bg_job_id = curr_id;

    for(pid_t child_pid : job.child_pids) {
        child_pid_to_job_id[child_pid] = curr_id;
    }

    bg_jobs.emplace(curr_id, std::move(job));
    return curr_id;
}

void report_and_remove_bg_job(int job_id) {
    PipelineResult& job = bg_jobs.at(job_id);

    fmt::print("Job's done, id: {}, pids: [ ", job_id);
    for(pid_t pid : job.child_pids) fmt::print("{} ", pid);
    fmt::print("], exit codes: [ ");
    for(int code : job.exit_statuses) fmt::print("{} ", code);
    fmt::print("]\n");

    for(pid_t pid : job.child_pids) {
        child_pid_to_job_id.erase(pid);
    }

    bg_jobs.erase(job_id);
}

void reap_bg_jobs() {
    // (bg job id, pid) pair

    std::queue<std::pair<int, pid_t>> pids_to_check;
    // enquue all pids to wait first, no need to find next in line later

    std::vector<int> bg_jobs_to_remove;
    for(auto& job_and_id : bg_jobs) {
        PipelineResult& curr_job = job_and_id.second;
        bool found_unfinished_process = false;
        for(size_t j=0; j<curr_job.child_pids.size(); j++) {
            if(!curr_job.finished[j]) {
                pids_to_check.push({ job_and_id.first, curr_job.child_pids[j] });
                found_unfinished_process = true;
            }
        }

        if(!found_unfinished_process)
            bg_jobs_to_remove.push_back(curr_job.bg_job_id);
    }

    for(int bg_job_to_remove : bg_jobs_to_remove)
        report_and_remove_bg_job(bg_job_to_remove);

    while(!pids_to_check.empty()) {
        auto [bg_job_id, pid] = pids_to_check.front();
        pids_to_check.pop();

        int wstatus;
        pid_t res = waitpid(pid, &wstatus, WNOHANG);
        if(!res) continue;
        PipelineResult& curr_job = bg_jobs.at(bg_job_id);

        int pid_idx = -1;
        bool found_unfinished_process = false;
        for(size_t j=0; j<curr_job.child_pids.size(); j++) {
            if(curr_job.child_pids[j] == pid) pid_idx = j;
            if(!curr_job.finished[j]) found_unfinished_process = true;
        }

        curr_job.finished[pid_idx] = true;
        curr_job.exit_statuses[pid_idx] = wstatus;

        if(!found_unfinished_process) {
            report_and_remove_bg_job(curr_job.bg_job_id);
        }
    }
}

// Thin wrapper around syscalls related to file descriptors
// Can actually hold 2 descriptors in case of pipes
struct FileDescriptor {
    // for regular files, only fd_read is nonzero
    // for pipes, both are nonzero
    int fd;
    int fd2;

    FileDescriptor() : fd(0), fd2(0) { }
    ~FileDescriptor() {
        close();
    }
    void close() {
        if(fd) ::close(fd);
        if(fd2) ::close(fd2);
        fd = 0;
        fd2 = 0;
    }

    // returns open() syscall result
    // sets errno on error
    int open(const char* path, int flags) {
        fd = ::open(path, flags);
        return fd;
    }
    int open(const char* path, int flags, int mode) {
        fd = ::open(path, flags, mode);
        return fd;
    }

    // returns pipe() syscall result
    // sets errno on error
    int create_pipe() {
        int fds[2];
        int rc = ::pipe(fds);
        fd = fds[0];
        fd2 = fds[1];

        return rc;
    }

    FileDescriptor dup() {
        FileDescriptor res;
        if(fd) {
            int nfd = ::dup(fd);
            res.fd = nfd;
        }

        if(fd2) {
            int nfd = ::dup(fd2);
            res.fd2 = nfd;
        }
        return res;
    }

    bool is_open() const { return fd > 0 || fd2 > 0; }

    // pipe_in is write end of a pipe
    // pipe_out is its read end
    int pipe_in() const { return fd2; }
    int pipe_out() const { return fd; }
};

// do not end with a null
std::vector<char*> parent_env;
std::vector<char*> shell_argv;
// creates copies of the arguments
void load_parent_env(char** argv, char** envp) {
    parent_env.clear();
    char** curr = envp;
    while(*curr) {
        parent_env.push_back(strdup(*curr++));
    }

    curr = argv;
    while(*curr) {
        shell_argv.push_back(strdup(*curr++));
    }
}

void unload_parent_env() {
    for(char* arg : parent_env) free(arg);
    for(char* arg : shell_argv) free(arg);
    parent_env.clear();
    shell_argv.clear();
}

// stdin, stdout, stderr can be 0 (not redirected if 0)
// does not close descriptors
// pgid can be 0, in which case a new process group is created with the same pid as the child process
// (return value becomes both child pid and child pgid)
// takes all_fds because it needs to vlose all the inherited fd s in the child process
// return value behaves like fork()
pid_t do_spawn_process(const std::string& executable, CmdParams& cmd, int pgid, RedirectIo redirect, const std::vector<int>& all_fds) {
    std::vector<const char*> argv;
    for(const std::vector<char>& arg : cmd.cmdline) {
        argv.push_back(arg.data());
    }
    argv.push_back(NULL);
    argv[0] = executable.c_str();

    // we modify the parent_env with new env vars
    // but restore it at the end of this function
    size_t old_env_length = parent_env.size();
    for(auto& var : cmd.env_vars) {
        std::vector<char>& key = var.first;
        std::vector<char>& value = var.second;

        key[key.size()-1]='=';
        for(char c : value) key.push_back(c);
        parent_env.push_back(key.data());
    }
    parent_env.push_back(NULL);

    pid_t pid = fork();
    if(pid < 0) {
        return pid;
    } else if(pid == 0) {
        // Child process. This function should not return error values but just exit

        if(redirect.stdin_fd) dup2(redirect.stdin_fd, STDIN_FILENO);
        if(redirect.stdout_fd) dup2(redirect.stdout_fd, STDOUT_FILENO);
        if(redirect.stderr_fd) dup2(redirect.stderr_fd, STDERR_FILENO);

        // Closing all the descriptors we inherited from the parent process
        // the one's we're using for io redirection have been dup2'd above
        for(size_t i=0; i<all_fds.size(); i++) close(all_fds[i]);

        if(execve(argv[0], (char**)argv.data(), (char**)parent_env.data())) {
            if(errno == ENOENT) {
                _exit(ENOENT);
            } else {
                fmt::print(stderr, "execve: {}\n", strerror(errno));
                _exit(errno);
            }
        }

        fmt::print(stderr, "Unreachable code\n");
        _exit(1);
    }

    if(!pgid) pgid = pid;

    int rc = setpgid(pid, pgid);
    parent_env.resize(old_env_length);
    if(rc < 0) return rc;

    return pid;
}

// sets errno if we can't execute the file
bool can_execute_file(const std::string& path) {
    struct stat stbuf;
    int rc = stat(path.c_str(), &stbuf);
    if(rc < 0) {
        // stat sets errno
        return false;
    }

    if(stbuf.st_uid == getuid()) {
        errno = (stbuf.st_mode & S_IXUSR) ? 0 : EACCES;
        return (errno == 0);
    }

    if(stbuf.st_gid == getgid()) {
        errno = (stbuf.st_mode & S_IXGRP) ? 0 : EACCES;
        return (errno == 0);
    }

    errno = (stbuf.st_mode & S_IXOTH) ? 0 : EACCES;
    return (errno == 0);
}

struct ForegroundGroup {
    // returns negative on error
    // sets errno
    int begin() {
        old_pgid = tcgetpgrp(STDIN_FILENO);
        if(old_pgid < 0) return old_pgid;

        // we need to ignore sigttou before messing with tcsetpgrp because calling it
        // while we are a bg process fires a sigttou and the shel process gets suspended
        // https://stackoverflow.com/a/63750359
        sigset_t ttou;
        sigaddset(&ttou, SIGTTOU);
        int rc = sigprocmask(SIG_BLOCK, &ttou, &old_sigmask);
        if(rc < 0) return rc;

        began = true;
        return 0;
    }

    int end() {
        if(!began) {
            errno = ENOTSUP;
            return -ENOTSUP;
        }

        if(donated) {
            int rc = tcsetpgrp(STDIN_FILENO, old_pgid);
            if(rc < 0) return rc;
            donated = false;
        }

        int rc = sigprocmask(SIG_SETMASK, &old_sigmask, NULL);
        if(rc < 0) return rc;

        began = false;
        return 0;
    }

    int donate(pid_t pgid) {
        if(!began) {
            errno = ENOTSUP;
            return -ENOTSUP;
        }
        int rc = tcsetpgrp(STDIN_FILENO, pgid);
        if(rc < 0) return rc;

        donated = true;
        return 0;
    }

    ~ForegroundGroup() {
        if(!began) return;
        end();
    }
private:
    bool began = false;
    bool donated = false;

    int old_pgid;
    sigset_t old_sigmask;
};

PipelineResult do_wait_for_fg_pipeline(std::string_view cmdline, std::vector<pid_t>&& pids, pid_t pgid) {
    std::vector<int> statuses;
    statuses.resize(pids.size());

    ForegroundGroup foreg;
    if(foreg.begin() < 0) return PipelineResult::perror("ForegroundGroup::begin");

    if(foreg.donate(pgid) < 0) return PipelineResult::perror("ForegroundGroup::donate");

    size_t num_finished = 0;
    for(size_t i=0; i<pids.size(); i++) {
        auto pid_status = do_wait_child();
        pid_t pid = pid_status.first;
        int status = pid_status.second;
        if(pid < 0) return PipelineResult::perror("waitpid");

        int pid_idx = -1;
        for(size_t j=0; j<pids.size(); j++) {
            if(pids[i] == pid) {
                pid_idx = j;
                break;
            }
        }

        if(pid_idx == -1) {
            // If the finished process is not in this job's pid list, find
            // the finished process's pid in the bg_jobs and mark it as done

            // This lookup should never fail
            int job_id = child_pid_to_job_id.at(pid);
            auto& job = bg_jobs.at(job_id);

            for(size_t j=0; j<job.child_pids.size(); j++) {
                if(job.child_pids[j] == pid) {
                    job.finished[j] = true;
                    job.exit_statuses[j] = status;
                    break;
                }
            }
            // retry
            i--; continue;
        }

        statuses[pid_idx] = pid_status.second;
        if(!WIFSTOPPED(pid_status.second)) num_finished++;
    }

    if(foreg.end() < 0) return PipelineResult::perror("ForegroundGroup::end");

    if(num_finished < pids.size()) return PipelineResult::background(cmdline, std::move(pids), pgid);

    return PipelineResult::success(cmdline, std::move(pids), std::move(statuses));
}

PipelineResult run_pipeline(CmdPipeline& pipeline, std::string_view cmdline) {
    if(pipeline.cmds.empty())
        return PipelineResult::with_error_message("No commands in pipeline");

    std::vector<pid_t> pids;

    pids.resize(pipeline.cmds.size());

    std::vector<std::string> found_executables;
    for(size_t i=0; i<pipeline.cmds.size(); i++) {
        // do not search relative paths
        bool contains_slash = false;
        for(char c : pipeline.cmds[i].cmdline[0]) {
            if(c == '/') contains_slash = true;
        }

        if(contains_slash) {
            std::string copied_path = pipeline.cmds[i].cmdline[0].data();
            if(!can_execute_file(copied_path))
                return PipelineResult::perror(copied_path, "can_execute_file");
            found_executables.push_back(copied_path);
            continue;
        }

        found_executables.push_back(Path::search_in_path(get_program_name(pipeline.cmds[i])));
        if(found_executables[i].empty())
            return PipelineResult::with_error_message(get_program_name(pipeline.cmds[i]), "No such command");
        if(!can_execute_file(found_executables[i]))
            return PipelineResult::perror(get_program_name(pipeline.cmds[i]), "can_execute_file");
    }

    FileDescriptor pipeline_stdin;
    FileDescriptor pipeline_stdout;
    FileDescriptor pipeline_stderr;

    // we need to hold on to these descriptors so that we can pass them to the child 
    // processes in the pipeline and let it close its other descriptors. If we don't
    // do it, processes in pipeline do not terminate
    // https://stackoverflow.com/questions/66672518/basic-pipe-does-not-terminate-in-c
    std::vector<int> all_fds;

    if(!pipeline.redirect_stdin.empty()) {
        pipeline_stdin.open((char*)pipeline.redirect_stdin.data(), O_RDONLY);
        if(!pipeline_stdin.is_open()) return PipelineResult::perror("open");
        all_fds.push_back(pipeline_stdin.fd);
    }

    if(!pipeline.redirect_stdout.empty()) {
        int open_flags = O_WRONLY | O_CREAT;
        if(pipeline.append_stdout) open_flags |= O_APPEND;
        else open_flags |= O_TRUNC;

        pipeline_stdout.open((char*)pipeline.redirect_stdout.data(), open_flags, 0644);
        if(!pipeline_stdout.is_open()) return PipelineResult::perror("open");
        all_fds.push_back(pipeline_stdout.fd);

        if(pipeline.redirect_stderr) {
            pipeline_stderr = pipeline_stdout.dup();
            all_fds.push_back(pipeline_stderr.fd);
        }
    }

    std::vector<FileDescriptor> pipes;
    pipes.resize(pipeline.cmds.size()-1);

    for(size_t i=0; i<pipeline.cmds.size()-1; i++) {
        int rc = pipes[i].create_pipe();
        if(rc < 0) return PipelineResult::perror("pipe");
        all_fds.push_back(pipes[i].fd);
        all_fds.push_back(pipes[i].fd2);
    }

    pid_t pgid = 0;

    for(size_t i=0; i<pipeline.cmds.size(); i++) {
        RedirectIo redirect;

        redirect.stdin_fd = (i == 0) ? pipeline_stdin.fd : pipes[i-1].pipe_out();
        redirect.stdout_fd = (i == pipeline.cmds.size()-1) ? pipeline_stdout.fd : pipes[i].pipe_in(); 
        redirect.stderr_fd = pipeline_stderr.fd;

        pids[i] = do_spawn_process(found_executables[i], pipeline.cmds[i], pgid, redirect, all_fds);
        if(pids[i] < 0) return PipelineResult::perror("do_spawn_process");
        if(!pgid) pgid = pids[i];
    }

    // we should close the pipes ourselves before waiting for child processes
    for(size_t i=0; i<pipeline.cmds.size()-1; i++) pipes[i].close();

    if(pipeline.bg_job) {
        // we send sigstop to processes we're launching in background
        // the processes that were suspended by ctrl z receive it from the 
        // user. When we're fg'ing them, we send a sigcont.

        // FIXME: This might be behaving differently in osx and linux. I remember adding it fixed
        // cat & behavior on osx, but removing it is necessary on linux
        // for(pid_t pid : pids) kill(pid, SIGST0P);
        return PipelineResult::background(cmdline, std::move(pids), pgid);
    }

    return do_wait_for_fg_pipeline(cmdline, std::move(pids), pgid);
}

void report_pipeline_result(const PipelineResult& res) {
    if(res.type == Bebish::PipelineResult::Type::Error) {
        fmt::print(stderr, "{}\n", res.error_message);
    } else if(res.type == Bebish::PipelineResult::Type::BgJob) {
        fmt::print("bg job [{}] {}\n", res.bg_job_id, res.cmdline);
        Bebish::bg_jobs.emplace(res.bg_job_id, std::move(res));
    } else {
        for(size_t i=0; i<res.child_pids.size(); i++) {
            // TODO: Get program name here
            if(!res.finished[i]) continue;
            fmt::print("Process {} returned {}\n", res.child_pids[i], res.exit_statuses[i]);
        }
    }
}

// returns true if any builtins were ran
bool do_builtins(const CmdParams& args) {
    if(args.cmdline.empty()) return false;
    const char* arg0 = args.cmdline[0].data();

    if(!strcmp(arg0, "cd")) {
        const char* arg1 = args.cmdline[1].data();
        int rc = chdir(arg1);
        if(rc < 0) {
            fmt::print(stderr, "cd {}: {}\n", arg1, strerror(errno));
        }
        return true;
    }

    if(!strcmp(arg0, "jobs")) {
        for(auto& job : bg_jobs) {
            fmt::print("job #{}, pids: [ ", job.first);
            for(auto pid : job.second.child_pids) fmt::print("{} ", pid);
            fmt::print("] {}\n", job.second.cmdline);
        }
        return true;
    }

    if(!strcmp(arg0, "fg")) {
        if(bg_jobs.empty()) {
            fmt::print(stderr, "fg: no background jobs\n");
            return true;
        }
        int job_id = 0;
        if(args.cmdline.size()>1) {
            const char* arg1 = args.cmdline[1].data();
            job_id = atoi(arg1); // returns 0 on invalid, which fails in the next step
            if(bg_jobs.find(job_id) == bg_jobs.end()) {
                fmt::print(stderr, "fg: No job with id #{}\n", job_id);
                return true;
            }
        } else {
            // TODO: Make the min-finding process less naive :)
            int min_job_id = INT_MAX;
            for(auto& job : bg_jobs) {
                if(job.first < min_job_id) min_job_id = job.first;
            }
            job_id = min_job_id;
        }

        PipelineResult& job = bg_jobs.at(job_id);
        for(pid_t pid : job.child_pids)
            kill(pid, SIGCONT);

        PipelineResult res = do_wait_for_fg_pipeline(job.cmdline, std::move(job.child_pids), job.pgid);
        report_pipeline_result(res);

        bg_jobs.erase(job_id);
        return true;
    }

    if(!strcmp(arg0, "exit")) {
        const char* arg1 = args.cmdline[1].data();
        if(!arg1) exit(0);

        int exit_code = atoi(arg1);
        if(!exit_code) exit_code = 1;
        exit(exit_code);

        return true;
    }

    if(!strcmp(arg0, "reload")) {
        auto path = get_current_executable();
        if(args.cmdline.size() > 1)
            path = args.cmdline[0].data();
        if(!path) {
            fmt::print(stderr, "reload: no executable name given\n");
            return true;
        }

        std::vector<char*> new_argv { Bebish::shell_argv };
        new_argv.push_back(NULL);
        if(execvp(path->c_str(), (char**)new_argv.data()) < 0) perror("execvp");
        return true;
    }

    return false;
}

PipelineResult PipelineResult::success(std::string_view cmdline, std::vector<pid_t>&& pids, std::vector<int>&& statuses) {
    return PipelineResult(cmdline, std::move(pids), std::move(statuses));
}

PipelineResult PipelineResult::background(std::string_view cmdline, std::vector<pid_t>&& pids, pid_t pgid) {
    return PipelineResult(cmdline, std::move(pids), pgid);
}

PipelineResult PipelineResult::perror(std::string_view cmd, std::string_view cause) {
    return PipelineResult(errno, fmt::format("{}: {}: {}", cmd, cause, strerror(errno)));
}

PipelineResult PipelineResult::with_error_message(std::string_view cmd, std::string_view message) {
    return PipelineResult(1, fmt::format("{}: {}", cmd, message));
}

PipelineResult PipelineResult::perror(std::string_view cause) {
    return PipelineResult(errno, fmt::format("{}: {}", cause, strerror(errno)));
}

PipelineResult PipelineResult::with_error_message(std::string_view message) {
    return PipelineResult(1, message);
}

PipelineResult::PipelineResult(std::string_view cmdline, std::vector<pid_t>&& pids, std::vector<int>&& statuses) : 
        type(Type::Finished),
        child_pids(pids),
        cmdline(cmdline) {
            for(auto status : statuses) {
                finished.push_back(WIFEXITED(status) || WIFSIGNALED(status));
                exit_statuses.push_back(WEXITSTATUS(status));
            }
        }

PipelineResult::PipelineResult(std::string_view cmdline, std::vector<pid_t>&& pids, pid_t pgid) :
        type(Type::BgJob),
        child_pids(pids),
        pgid(pgid),
        cmdline(cmdline) {
            for(size_t i=0; i<pids.size(); i++) {
                finished.push_back(false);
                exit_statuses.push_back(-1);
            }

            // TODO: This looks awkward, fix it
            create_bg_job(std::move(*this));
        }

PipelineResult::PipelineResult(int error_code, std::string_view message) :
        type(Type::Error),
        error_code(error_code),
        error_message(message) { }

}